from django.shortcuts import render
from django.shortcuts import render, redirect
from .models import jenisKegiatan,namaOrang
from .forms import FormjenisKegiatan,FormOrang
from django.views.decorators.http import require_POST

# Create your views here.
def index(request):
    nama_orang_list = namaOrang.objects.order_by('id')
    formOrang = FormOrang()
    nama_kegiatan = jenisKegiatan.objects.order_by('id')
    formKeg = FormjenisKegiatan()
    context = {'nama_orang_list':nama_orang_list, 'formOrang':formOrang, 'nama_kegiatan':nama_kegiatan,'formKeg':formKeg}
    return render (request, 'home/index.html', context)

@require_POST
def addKegiatan(request):
    form = FormjenisKegiatan(request.POST)
    if form.is_valid():
        newKegiatan = jenisKegiatan(namaKegiatan = request.POST['namaKegiatan'])
        newKegiatan.save()
    return redirect('story6:index')

def addOrang(request):
    form = FormOrang(request.POST)
    if form.is_valid():
        newOrang = namaOrang(namaOrang = request.POST['namaOrang'])
        newOrang.save()
    return redirect('story6:index')


# Create your views here.

