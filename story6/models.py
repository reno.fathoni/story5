from django.db import models


# Create your models here.
class jenisKegiatan(models.Model):
    namaKegiatan = models.CharField(max_length=100)

    def __str__(self):
        return self.namaKegiatan

class namaOrang(models.Model):
    namaOrang = models.CharField(max_length=100)
    kegiatan = models.ForeignKey(jenisKegiatan,null=True,blank=True,on_delete=models.CASCADE)
    
    def __str__ (self):
        return self.namaOrang
