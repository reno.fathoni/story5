from django import forms

class FormjenisKegiatan(forms.Form):
    namaKegiatan = forms.CharField(max_length=100,label="Kegiatan ?" ,widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Apa Kegiatan mu ?' }))

class FormOrang(forms.Form):
    namaOrang = forms.CharField(max_length=100,label="Nama ?", widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Nama mu siapa ?' }))