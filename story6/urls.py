from django.urls import path

from . import views

app_name = 'story6'

urlpatterns = [
    path('', views.index, name='index'),
    path('addKegiatan/',views.addKegiatan, name ='addKegiatan'),
    path('addOrang/',views.addOrang, name ='addOrang'),
]