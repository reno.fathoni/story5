
from django.http import request
from django.shortcuts import render, redirect
from .models import jadwal
from .forms import jadwalForm
from django.views.decorators.http import require_POST



def index(request):
    jadwal_list = jadwal.objects.order_by('id')
    form = jadwalForm()
    context = {'jadwal_list': jadwal_list, 'form':form}
    return render(request, 'home/index.html', context)

def detail(request, jadwal_id):
    jadwalSelected = jadwal.objects.get(id=jadwal_id)
    response = {'jadwalSelected': jadwalSelected}
    return render(request,'home/detail.html', response)

@require_POST
def addJadwal(request):
    form = jadwalForm(request.POST)
    if form.is_valid():
        newJadwal = jadwal(mataKuliah=request.POST['mataKuliah'], namaDosen=request.POST['namaDosen'],jumlahSKS=request.POST['jumlahSKS'],deskMatkul=request.POST['deskMatkul'],ruangKelas=request.POST['ruangKelas'],tahun=request.POST['tahun'])
        newJadwal.save()

    return redirect('story5:index')

def deleteJadwal(request, jadwal_id):
    jadwal.objects.get(id=jadwal_id).delete()
    return redirect('story5:index')