from django.urls import path

from . import views

app_name = 'story5'

urlpatterns = [
    path('', views.index, name='index'),
    path('addJadwal/',views.addJadwal, name ='addJadwal'),
    path('deleteJadwal/<jadwal_id>',views.deleteJadwal, name ='deleteJadwal'),
    path('detail/<jadwal_id>', views.detail, name='detail'),
]