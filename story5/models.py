from django.db import models


# Create your models here.
class jadwal(models.Model):
    mataKuliah = models.CharField(max_length=40)
    namaDosen = models.CharField(max_length=40)
    jumlahSKS = models.IntegerField()
    deskMatkul = models.CharField(max_length=1000, default='')
    tahun = models.CharField(max_length=40, default='')
    ruangKelas = models.CharField(max_length=40, default='')    

    def __str__(self):
        return self.mataKuliah
