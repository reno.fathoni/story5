from django import forms

class jadwalForm(forms.Form):
    jumlah_SKS= [
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('5', '5'),
    ('6', '6'),
    ]
    mataKuliah = forms.CharField(max_length=40, widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Enter your Matkul' }))
    namaDosen = forms.CharField(max_length=40, widget=forms.TextInput(attrs={'class' : 'form-control', 'placeholder' : 'Siapa dosennya ?'}))
    deskMatkul = forms.CharField(max_length=1000, widget=forms.Textarea(attrs={'class' : 'form-control', 'id':'exampleFormControlTextarea1','rows':'5', 'null':'True'}))
    ruangKelas = forms.CharField(max_length=40, widget=forms.TextInput(attrs={'class' : 'form-control'}))
    tahun = forms.CharField(max_length=40, widget=forms.TextInput(attrs={'class' : 'form-control'}))
    jumlahSKS = forms.IntegerField(max_value=100, widget=forms.Select(choices=jumlah_SKS))